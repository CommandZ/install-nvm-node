#!/usr/bin/env sh

# NVM and NodeJS Easy Installer Version 1.0.0
# Built for Pixel & Line (http://pixelandline.com)
# Chris Coggburn <chris@pixelandline.com>

# Choose which version of NodeJS you want to install from NVM (string)
# DEFAULT: NODEVER="node" (latest stable version)
# EXAMPLE: NODEVER="4.2" (latest version in 4.2 branch)
NODEVER="node"

# Install optional common global modules? (boolean)
# DEFAULT: INSTEXTRA=true
INSTEXTRA=true

# What extras should be installed?
# DEFAULT: EXTRAS="gulp-cli"
EXTRAS="gulp-cli"

echo "---------------------------------------------------------------------------------"
echo "Installing NVM ( https://github.com/creationix/nvm )"
echo

if hash curl 2>/dev/null; then
  curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.30.1/install.sh | bash
else
  wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.30.1/install.sh | bash
fi

echo "---------------------------------------------------------------------------------"
echo
echo "Sourcing NVM so you don't need to restart your terminal. ( source ~/.nvm/nvm.sh )"
echo
source ~/.nvm/nvm.sh

echo "---------------------------------------------------------------------------------"
echo
echo "Installing NodeJS Version '${NODEVER}' ( nvm install ${NODEVER} )"
echo
nvm install ${NODEVER}

echo "---------------------------------------------------------------------------------"
echo
echo "Setting installed NodeJS as default ( nvm alias default ${NODEVER} )"
echo
nvm alias default ${NODEVER}

echo "---------------------------------------------------------------------------------"
echo
echo "Updating NPM to latest version ( npm install -g npm )"
echo
npm install -g npm

if [ $INSTEXTRA = true ]; then
  echo "---------------------------------------------------------------------------------"
  echo
  echo "Installing commonly used global modules ( npm install -g ${EXTRAS} )"
  echo
  npm install -g ${EXTRAS}
fi

echo "---------------------------------------------------------------------------------"
echo
echo
echo "NodeJS: $(node -v)"
echo "NPM:    $(npm -v)"
echo
